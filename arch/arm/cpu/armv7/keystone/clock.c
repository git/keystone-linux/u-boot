/*
 * Copyright (C) 2012 Texas Instruments.
 *
 * Keystone: Clock management APIs
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <common.h>
#include <asm-generic/errno.h>
#include <asm/io.h>
#include <asm/processor.h>
#include <asm/arch/clock.h>
#include <asm/arch/clock_defs.h>

static void pll_delay(unsigned int loopCount)
{
	while (loopCount--) {
		asm("   NOP");
	}
}

static void wait_for_completion(const struct pll_init_data *data)
{
	volatile int i;
	for (i = 0; i < 100; i++) {
		pll_delay(300);
		if ( (pllctl_reg_read(data->pll, stat) & 0x00000001) == 0 ) {
			break;
		}
	}
}

struct pll_regs {
	u32	reg0, reg1;
};


#ifdef CONFIG_SOC_K2HK
#include "clock-k2hk.c"
#endif

#ifdef CONFIG_SOC_K2E
#include "clock-k2e.c"
#endif

#ifdef CONFIG_SOC_K2L
#include "clock-k2l.c"
#endif

void init_pll(const struct pll_init_data *data)
{
	u32 tmp, tmp_ctl, pllm, plld, pllod, bwadj;

	pllm = data->pll_m - 1;
	plld = (data->pll_d - 1) & 0x3f;
	pllod = (data->pll_od - 1) & 0xf;

	if (data->pll == MAIN_PLL) {
		pll_delay(140000);

		/* if RBL configures the PLL, the BYPASS bit would be set to '0'           */
		tmp = pllctl_reg_read(data->pll, secctl);

		if (tmp & (PLLCTL_BYPASS)) {
			/* PLL BYPASS is enabled, we assume if not in Bypass ENSAT = 1 */

			reg_setbits( pll_regs[data->pll].reg1, BIT(MAIN_ENSAT_OFFSET));

			pllctl_reg_clrbits(data->pll, ctl, PLLCTL_PLLEN | PLLCTL_PLLENSRC);
			pll_delay(225); /* Wait for 4 RefClks */

			pllctl_reg_setbits(data->pll, secctl, PLLCTL_BYPASS);
			pllctl_reg_setbits(data->pll, ctl, PLLCTL_PLLPWRDN);
			pll_delay(14000);

			/* Power up the PLL */
			pllctl_reg_clrbits(data->pll, ctl, PLLCTL_PLLPWRDN);
		}
		else {
			pllctl_reg_clrbits(data->pll, ctl, PLLCTL_PLLEN | PLLCTL_PLLENSRC);
			pll_delay(225);
		}

		/* Set pll multipler (13 bit field) */

		/* Set the PLLM[5:0] to the PLL controller PLLM register */
		pllctl_reg_write(data->pll, mult, pllm & 0x3f);

		reg_rmw(pll_regs[data->pll].reg0, 0x0007F000, (pllm << 6));

		/* Set the BWADJ     (12 bit field)  */
		tmp_ctl = pllm >> 1; /* Divide the pllm by 2 */
		reg_rmw(pll_regs[data->pll].reg0, 0xFF000000, (tmp_ctl << 24));
		reg_rmw(pll_regs[data->pll].reg1, 0x0000000F, (tmp_ctl >> 8));

		/* Set the pll divider (6 bit field)                                         *
		 * PLLD[5:0] is located in MAINPLLCTL0                                       */
		reg_rmw(pll_regs[data->pll].reg0, 0x0000003F, plld);

		/* Set the OUTPUT DIVIDE (4 bit field) in SECCTL */
		pllctl_reg_rmw(data->pll, secctl, 0x00780000, (pllod << 19));
		wait_for_completion(data);

		pllctl_reg_write(data->pll, div1, 0x00008000);
		pllctl_reg_write(data->pll, div2, 0x00008000);
		pllctl_reg_write(data->pll, div3, 0x00008001);
		pllctl_reg_write(data->pll, div4, 0x00008004);
		pllctl_reg_write(data->pll, div5, 0x00008017);

		pllctl_reg_setbits(data->pll, alnctl, 0x1f);

		/* Set GOSET bit in PLLCMD to initiate the GO operation to change the divide */
		pllctl_reg_setbits(data->pll, cmd, 0x1);
		pll_delay(1000); /* wait for the phase adj */
		wait_for_completion(data);

		/* Reset PLL */
		pllctl_reg_setbits(data->pll, ctl, PLLCTL_PLLRST);
		pll_delay (14000);		/* Wait for a minimum of 7 us*/
		pllctl_reg_clrbits(data->pll, ctl, PLLCTL_PLLRST);
		pll_delay (70000);	/* Wait for PLL Lock time (min 50 us) */

		/* Release Bypass */
		pllctl_reg_clrbits(data->pll, secctl, PLLCTL_BYPASS);

		/* Set the PLLEN */
		tmp = pllctl_reg_setbits(data->pll, ctl, PLLCTL_PLLEN);
	}
#ifndef CONFIG_SOC_K2E
	else if (data->pll == TETRIS_PLL) {
		bwadj = pllm >> 1;

		/* Enable glitch-free Bypass */
#ifdef CONFIG_SOC_K2HK
		/* ARM_PLL_EN in CHIP_MISC_CTL1 for Kepler/Hawking */
		reg_clrbits(0x02620c7c, (1 << 13));
#else
		/* ARMPLLCTL1[15] for K2L */
		reg_clrbits(pll_regs[data->pll].reg1 , (1<<15));
#endif

		/* Set ENSAT for proper PLL operation in ARMPLLCTL1 */
		reg_setbits(pll_regs[data->pll].reg1 , (1 << 6));

		/* Enable Bypass in ARMPLLCTL0 */
		reg_setbits(pll_regs[data->pll].reg0,  0x00800000);

		/* Program PLLM, PLLD, PLLOD, BWADJ[7:0],
		 * and keep Bypass enabled in ARMPLLCTL0*/
		tmp = ((bwadj & 0xff) << 24) |
		       (pllm << 6)           |
		       (plld & 0x3f)         |
		       (pllod << 19)         |
		       (1 << 23);
		__raw_writel(tmp, pll_regs[data->pll].reg0);

		/* Set BWADJ[11:8] bits in ARMPLLCTL1 */
		tmp = __raw_readl(pll_regs[data->pll].reg1);
		tmp &= ~(0xf);
		tmp |= ((bwadj>>8) & 0xf);
		__raw_writel(tmp, pll_regs[data->pll].reg1);

		/* Place PLL into reset by PLLRST in ARMPLLCTL1 */
		reg_setbits(pll_regs[data->pll].reg1 , (1<<14));

		/* Wait for at least 5 us based on the reference clock */
		pll_delay(21000); /* PLL reset time */

		/* Release PLL reset */
		reg_clrbits(pll_regs[data->pll].reg1 , (1<<14));

		/* Wait for at least 500 * REFCLK cycles * (PLLD + 1) */
		pll_delay(105000); /* PLL lock time */

		/* Disable PLL Bypass */
		reg_clrbits(pll_regs[data->pll].reg0,  0x00800000);

		/* Disable glitch-free Bypass */
#ifdef CONFIG_SOC_K2HK
		/* ARM_PLL_EN in CHIP_MISC_CTL1 for Kepler/Hawking */
		reg_setbits(0x02620c7c, (1 << 13));
#else
		/* ARMPLLCTL1[15] for K2L */
		reg_setbits(pll_regs[data->pll].reg1 , (1<<15));
#endif
	}
#endif
	else {

		reg_setbits(pll_regs[data->pll].reg1, 0x00000040); /* Set ENSAT bit = 1 */
		/* process keeps state of Bypass bit while programming all other DDR PLL settings */
		tmp = __raw_readl(pll_regs[data->pll].reg0);
		tmp &= 0x00800000;	/* clear everything except Bypass */

		/* Set the BWADJ[7:0], PLLD[5:0] and PLLM to PLLCTL0, bypass disabled */
		bwadj = pllm >> 1;
		tmp |= ((bwadj & 0xff) << 24) | (pllm << 6) | (plld & 0x3f) | (pllod<<19);
		__raw_writel(tmp, pll_regs[data->pll].reg0);

		/* Set BWADJ[11:8] bits */
		tmp = __raw_readl(pll_regs[data->pll].reg1);
		tmp &= ~(0xf);
		tmp |= ((bwadj>>8) & 0xf);

		__raw_writel(tmp, pll_regs[data->pll].reg1);


		/* Reset bit: bit 14 for both DDR3 & PASS PLL */
		tmp = 0x00004000;
		/* Set RESET bit = 1 */
		reg_setbits(pll_regs[data->pll].reg1, tmp);
		/* Wait for a minimum of 7 us*/
		pll_delay(14000);
		/* Clear RESET bit */
		reg_clrbits(pll_regs[data->pll].reg1, tmp);
		pll_delay(70000);

		reg_clrbits(pll_regs[data->pll].reg0, 0x00800000); /* clear BYPASS (Enable PLL Mode) */
		pll_delay(14000);	/* Wait for a minimum of 7 us*/
	}

	pll_delay (140000);
}

void init_plls(int num_pll, struct pll_init_data *config)
{
	int i;

	for (i = 0; i < num_pll; i++)
		init_pll(&config[i]);
}

void pll_pa_clk_sel(int pa_pll)
{
	u32 reg = readl(pll_regs[PASS_PLL].reg1);

	if (pa_pll)
		reg |= 0x00002000; /* set PLL Select (bit 13) for PASS PLL */
	else
		reg &= ~(0x00002000); /* clear PLL Select for MAIN PLL */

	writel(reg, pll_regs[PASS_PLL].reg1);

	pll_delay(1000);
}


